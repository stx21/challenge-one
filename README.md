## **Challenge-one**

### [Gitlab Pages](https://stx21.gitlab.io/challenge-one/)

`Challenges for week:3 - create a code/script to answer the challenges.`

The challenges related to:

1. HTML & CSS
2. GIT
3. OOP
4. Flowchart, Pseudocode, Algorithm
5. Promis & Async-Await
