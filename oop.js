// ada sebuah mobil berwarna merah berkecapatan 250 per jam 
// ada sebuah motor berwarna biru beroda dua berkecepatan 125 km per jam

class Kendaraan {
    constructor (warna, kecepatan) {
        this.warna = warna;
        this.kecepatan = kecepatan;
        this.name = this.constructor.name
    } 
    //getName =() => console.log(`ada sebuah ${this.name}`);
    //deskripsi =() => console.log(`berwarna ${this.warna} berkecepatan ${this.kecepatan}`);
    getName =() =>  `ada sebuah ${this.name}`;
    deskripsi =() => `berwarna ${this.warna} berkecepatan ${this.kecepatan}`;
}

// console.log(Kendaraan);

class Mobil extends Kendaraan {
    constructor (warna, kecepatan) {
        super (warna, kecepatan)
    }
}

class Motor extends Kendaraan {
    constructor (warna, kecepatan) {
        super (warna, kecepatan)
        this.roda = 'dua';
    }
}

const mobilbaru = new Mobil ('merah','250 km/h' );
// console.log(mobilbaru);
//mobilbaru.getName();
//mobilbaru.deskripsi();

const motorbaru = new Motor ('biru', '125 km/h')
// console.log(motorbaru);
// console.log(motorbaru.getName());
//motorbaru.deskripsi();


document.getElementById("kendaraan").innerHTML = mobilbaru.getName() + " " + mobilbaru.deskripsi() + "<br>" + motorbaru.getName() + " " + motorbaru.deskripsi();